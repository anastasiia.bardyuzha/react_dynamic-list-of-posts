import './App.css';

const App = () => (
  <div className="App">
    <h1>Dynamic list of posts</h1>
  </div>
);

export default App;

## Task
Basing on [Static list of posts](https://gitlab.com/anastasiia.bardyuzha/react_static-list-of-posts)
create the App

1. Initially `PostList` shows a `Load` button.
1. After a click disable the button, change its text to `Loading...` and download the data.
1. Once the data has been loaded, hide the button and display the list of posts instead.
1. Add a text field, to filter the posts by `post.title` and `post.body`.
1. (*) Add `debounce` with `1s` delay to the text field
module.exports = {
  root: true,
  env: {
    es6: true,
    browser: true
  },
  extends: [
    'airbnb',
    'react-app'
  ],
  parserOptions: {
    sourceType: 'module'
  },
  settings: {
    'import/resolver': {
      node: {
        paths: [
          '.'
        ],
        extensions: [
          '.js',
          '.jsx',
          '.ts',
          '.tsx'
        ]
      }
    }
  },
  rules: {
    'comma-dangle': 'off',
    'jsx-a11y/anchor-is-valid': 'off',
    'max-len': ['error', {
      code: 120,
      comments: 120,
      ignoreTemplateLiterals: true,
    }],
    'no-console': 'off',
    'no-plusplus': 'off',
    'no-restricted-syntax': ['warn', 'FunctionExpression', 'LabeledStatement', 'WithStatement'],
    'react/jsx-filename-extension': 'off',
    'react/jsx-props-no-spreading': 'off',
    'react/react-in-jsx-scope': 'off',
    'react/require-default-props': 'off',
    'no-param-reassign': 'off',
    'no-unused-vars': 'off',
    'import/extensions': [
      'error', 'never', { ignorePackages: true }
    ],
    'react/prop-types': 'off',
    'import/prefer-default-export': 'off',
    'react/jsx-max-props-per-line': [1, { maximum: 1, when: 'always' }],
    'no-shadow': 'off',
    'react/function-component-definition': [2, { namedComponents: 'arrow-function' }],
    '@typescript-eslint/no-shadow': ['error']
  }
};
